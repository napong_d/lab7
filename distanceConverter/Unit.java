package distanceConverter;

public interface Unit {

	double convertTo(double amount,Unit unit);

	double getValue();
	
	String toString();
	

}
